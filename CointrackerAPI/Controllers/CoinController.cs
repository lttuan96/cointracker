﻿using CointrackerAPI.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CointrackerAPI.Controllers
{
    [Route("/")]
    [ApiController]
    public class CoinController : ControllerBase
    {
        private List<Coin> CoinList;
        public CoinController()
        {
            this.CoinList = new List<Coin>
            {
                new Coin {Name="Bitcoin", CurrentPrice=0.0001f, ImageURL=""},
                new Coin {Name="Dogecoin", CurrentPrice=0.0002f, ImageURL=""},
                new Coin {Name="ETH", CurrentPrice=0.0003f, ImageURL=""}
            };
        }
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(this.CoinList);
        }
    }
}
