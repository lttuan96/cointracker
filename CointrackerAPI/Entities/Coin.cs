﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CointrackerAPI.Entities
{
    public class Coin
    {
        public string Name { get; set; }
        public float CurrentPrice { get; set; }
        public string ImageURL { get; set; }
    }
}
